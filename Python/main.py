
import requests
import time
import json
import threading

URL = "https://first-test-89e8d-default-rtdb.firebaseio.com/test.json"




def sendData(data):
    jsonPayloadString =  '{{ "data": {0} }}'.format(data)
    jsonPayload = json.loads(jsonPayloadString)

    r = requests.post(URL, json=jsonPayload)   
    print("{0} -> {1}".format(r.status_code, data)) 



def main ():   

    counter = 0

    while True:
        
        t = threading.Thread(target = sendData, args= (counter,))
        t.start()
        counter += 1
        time.sleep(0.1)

    pass




main ()