package com.example.todolist.bottomnavfragments.newToDo

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.todolist.DatabaseViewModel
import com.example.todolist.R
import com.example.todolist.UserViewModel
import com.example.todolist.databinding.FragmentToDoDetailsBinding
import com.example.todolist.model.Date_t
import com.example.todolist.model.ToDoItem

class ToDoDetailsFragment : Fragment() {



    private lateinit var _binding: FragmentToDoDetailsBinding
    private val binding get() = _binding
    private var itemID: Int = 0

    private var TAG = "detailsFragment"

    private lateinit var databaseViewModel: DatabaseViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        _binding = FragmentToDoDetailsBinding.inflate(inflater, container, false)

        databaseViewModel = ViewModelProvider(requireActivity()).get(com.example.todolist.DatabaseViewModel::class.java)

        val todo_done: Int? = arguments?.getInt("todo_done")
        val id : Int? = arguments?.getInt("ID")
        Log.d(TAG, "$id")
        Log.d(TAG, "$todo_done")

        if (id == null || todo_done == null || todo_done > 1){
            findNavController().popBackStack()
        }
        itemID = id!!

        var todoItem: ToDoItem

        todoItem = if(todo_done==0) databaseViewModel.localToDoList[itemID]
        else databaseViewModel.localDoneToDoList[itemID]

        val date: Date_t = todoItem.date
        binding.detailsDateTextView.text = "${date.day}/${date.month}/${date.year}"
        binding.detailsTitleTextView.text = todoItem.title
        binding.detailsDescriptionTextView.text = todoItem.description

        return binding.root
    }
}