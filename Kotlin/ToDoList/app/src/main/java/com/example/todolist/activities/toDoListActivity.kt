package com.example.todolist.activities

import android.content.Context
import android.graphics.Canvas
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.*
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.DatabaseViewModel
import com.example.todolist.R
import com.example.todolist.UserViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView


class toDoListActivity : AppCompatActivity() {


    lateinit var viewModel: DatabaseViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_to_do_list)

        viewModel = ViewModelProvider(this).get(DatabaseViewModel::class.java)
        
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        val navController = findNavController(R.id.fragment)
        val appBarConfiguration = AppBarConfiguration(setOf(R.id.toDoListFragment, R.id.closedTodoFragment, R.id.calendarFragment, R.id.accountFragment))


        setupActionBarWithNavController(navController, appBarConfiguration)
        bottomNavigationView.setupWithNavController(navController)

    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun logout(){
        viewModel.logoutUser()
        finish()
    }

    override fun onDestroy() {
        viewModel.logoutUser()
        super.onDestroy()
    }
}
