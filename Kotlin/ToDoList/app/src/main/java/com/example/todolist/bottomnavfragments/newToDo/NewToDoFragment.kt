package com.example.todolist.bottomnavfragments.newToDo

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.SeekBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.todolist.DatabaseViewModel
import com.example.todolist.R
import com.example.todolist.UserViewModel
import com.example.todolist.databinding.FragmentNewToDoBinding
import com.example.todolist.model.Date_t
import com.example.todolist.model.ToDoItem
import kotlinx.android.synthetic.main.fragment_new_to_do.*
import java.time.Instant
import java.time.ZoneId

class NewToDoFragment : Fragment() {


    private lateinit var _binding: FragmentNewToDoBinding
    private val binding get() = _binding

    private val userViewModel: DatabaseViewModel by viewModels()

    private var priority: Int = 1
    private var date: Date_t = Date_t()

    private val TAG = "NewToDoFragment"


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_save -> {

            val title = binding.Title.text.toString()
            val description = binding.Description.text.toString()

            val newToDo: ToDoItem = ToDoItem(title, description, priority, date)

            userViewModel.addNewToDoItem(newToDo) { success ->
                if(success) {
                    Navigation.findNavController(binding.root).popBackStack()
                }
                else {
                    Toast.makeText(context, "Errore", Toast.LENGTH_SHORT).show()
                }
            }
            true
        }
        else -> {
            Navigation.findNavController(binding.root).popBackStack()
            true
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.save_menu, menu)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        _binding = FragmentNewToDoBinding.inflate(inflater, container, false)



       _binding.PriorityseekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                priority = progress+1
                binding.priorityTextView.text = priority.toString()
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })

        val timestamp = binding.calendarView.date
        val dt = Instant.ofEpochSecond(timestamp/1000)
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime()

        date.day = dt.dayOfMonth
        date.month = dt.monthValue
        date.year = dt.year

        binding.calendarView.setOnDateChangeListener() { view, year, month, dayOfMonth ->

            date.day = dayOfMonth
            date.month = month+1
            date.year = year
        }


        activity?.title = "Add New To Do"


        return binding.root
    }

}