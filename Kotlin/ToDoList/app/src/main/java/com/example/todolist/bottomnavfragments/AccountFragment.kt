package com.example.todolist.bottomnavfragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.example.todolist.DatabaseViewModel
import com.example.todolist.R
import com.example.todolist.activities.MainActivity
import com.example.todolist.activities.toDoListActivity
import com.example.todolist.databinding.FragmentAccountBinding

class AccountFragment : Fragment() {


    private lateinit var _binding: FragmentAccountBinding
    private val binding get() = _binding

    private lateinit var databaseViewModel: DatabaseViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        databaseViewModel = ViewModelProvider(requireActivity()).get(com.example.todolist.DatabaseViewModel::class.java)
        _binding = FragmentAccountBinding.inflate(inflater, container, false)

        binding.LogoutTextView.setOnClickListener() {
            (activity as toDoListActivity).logout()
        }

        binding.LastnamTextView.text = databaseViewModel.user.last_name
        binding.NameTextView.text = databaseViewModel.user.name
        binding.UsernameTextView.text = databaseViewModel.user.email

        return binding.root
    }
}