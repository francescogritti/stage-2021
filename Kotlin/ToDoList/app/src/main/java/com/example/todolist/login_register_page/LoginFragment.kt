package com.example.todolist.login_register_page

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.todolist.R
import com.example.todolist.UserViewModel
import com.example.todolist.activities.toDoListActivity
import com.example.todolist.databinding.FragmentLoginBinding


class LoginFragment : Fragment() {


    private lateinit var _binding: FragmentLoginBinding
    private val binding get() = _binding

    private val userViewModel: UserViewModel by viewModels()

    private val TAG = "LoginFragment"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG,"start")
        Log.d(TAG, userViewModel.isUserLogged().toString())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        _binding = FragmentLoginBinding.inflate(inflater, container, false)

        binding.registerTextView.setOnClickListener {

            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_loginFragment_to_registerFragment) }
        }
        Log.d(TAG, userViewModel.isUserLogged().toString())
        if (userViewModel.isUserLogged()) {
            val intent = Intent(context, toDoListActivity::class.java)
            context?.startActivity(intent)
        }


        binding.loginButton.setOnClickListener {

            var email : String = binding.usernameTextField.text.toString()
            var password : String = binding.passwordTextField.text.toString()

            if(!email.isEmpty() && !password.isEmpty()) {

                userViewModel.login(email, password) { success ->
                    Log.d(TAG, success.toString())
                    if (success) {
                        val intent = Intent(context, toDoListActivity::class.java)
                        intent.putExtra("user_uid", userViewModel.user.uid)
                        context?.startActivity(intent)
                    }
                }
            }

            else{
                Toast.makeText(context, "Compilare tutti i campi", Toast.LENGTH_SHORT).show()
            }
        }

        return binding.root
    }

}