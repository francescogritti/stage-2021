package com.example.todolist

import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.todolist.model.ToDoItem
import com.example.todolist.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*


class DatabaseViewModel: ViewModel() {

    private val TAG = "DatabaseViewModel"

    private var _user: User? = null
    val user: User get() = _user!!

    private var currentUser: FirebaseUser? = null

    private lateinit var auth: FirebaseAuth
    private lateinit var databaseReference: DatabaseReference
    private var userDatabaseReference: DatabaseReference? = null


    private var getUserInfoCallback: ((Boolean) -> Unit)? = null


    private var onCreateCompleteCallback: ((Boolean)->Unit)? = null

    private var deleteItemCallback: ((Boolean)->Unit)? = null

    private var getListActionsCallback: ((MutableList<ToDoItem>)->Unit)? = null

    private var onFreeIDFoundCallback: ((ToDoItem, Int, String) -> Unit)? = null
    private var toDoToAdd: ToDoItem? = null

    private var _toDoItemsList = mutableListOf<ToDoItem>()
    val toDosList: MutableList<ToDoItem> get() = _toDoItemsList


    var localToDoList: MutableList<ToDoItem> = mutableListOf()
    var localDoneToDoList: MutableList<ToDoItem> = mutableListOf()


    init {
        Log.d(TAG, "New DatabaseViewModel created")

        // get firebase auth reference
        auth = FirebaseAuth.getInstance()

        // get database root reference
        databaseReference = FirebaseDatabase.getInstance().reference

        // user needs to be logged, otherwise an error had to be occurred while login
        if(auth.currentUser != null) {
            val userID = auth.currentUser!!.uid
            userDatabaseReference = databaseReference.child("User").child(userID)

            //syncLocalLists()
        }
    }

    override fun onCleared() {
        super.onCleared()
    }


    fun logoutUser (){
        Log.d(TAG, "Logout, Bye!")
        auth.signOut()
    }

    fun getUserInfo (callback: ((Boolean) -> Unit)? = null) {
        Log.d(TAG, "Get User Info")
        getUserInfoCallback = callback
        userDatabaseReference?.child("info")?.addListenerForSingleValueEvent(getUserInfoListener)
    }

    fun isUserLogged() : Boolean {
        if (auth.currentUser != null) {
            return true
        }
        return false
    }


    private val getUserInfoListener = object : ValueEventListener {

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val loggedUser = dataSnapshot.getValue(User::class.java)
            _user = loggedUser
            _user?.uid = auth.currentUser?.uid
            Log.d(TAG, user.toString())

            getUserInfoCallback?.invoke(true)
            getUserInfoCallback = null
        }

        override fun onCancelled(databaseError: DatabaseError) {
            // Getting Post failed, log a message
            Log.d(TAG, "loadPost:onCancelled", databaseError.toException())
            getUserInfoCallback?.invoke(false)
            getUserInfoCallback = null
        }
    }






    private fun syncLocalLists () {

        // sync both to do and done list
        getToDoItemsList(null)
        getDoneToDoItemsList(null)
    }


    // -------------------- TO DO ADD/DELETE/MOVE  METHODS -------------------- //


    // ---------------------------------------------------------------------------------------------------- //
    // ---------------------------------------- ADD NEW TO DO ITEM ---------------------------------------- //
    // ---------------------------------------------------------------------------------------------------- //

    fun addNewToDoItem (newToDo: ToDoItem, callback: (Boolean) -> Unit) : Boolean {

        if (userDatabaseReference == null) {
            return false
        }
        Log.d(TAG, "Add new to do item")

        onFreeIDFoundCallback = createToDoItemWithID
        onCreateCompleteCallback = callback
        toDoToAdd = newToDo
        getFirstFreeID()

        return true
    }


    fun getToDoItemWithID (ID: Int): ToDoItem? {

        for (toDoItem in localToDoList) {
            if(toDoItem.id == ID)return toDoItem
        }
        for (toDoItem in localDoneToDoList){
            if(toDoItem.id == ID)return toDoItem
        }
        return null
    }


    fun moveInDoneToDoItemWithID(ID: Int, callback: ((Boolean) -> Unit)?){

        Log.d(TAG, "Move in done $ID")
        val toDo = getToDoItemWithID(ID) ?: return

        deleteToDoItem(ID) {}
        onCreateCompleteCallback = callback
        createToDoItemWithID(toDo, ID, "done")
    }

    fun moveInToDoItemWIthID(ID: Int, callback: ((Boolean)->Unit)?) {
        Log.d(TAG, "Move in to do $ID")
        val toDo = getToDoItemWithID(ID) ?: return

        deleteToDoItem(ID) {}
        onCreateCompleteCallback = callback
        createToDoItemWithID(toDo, ID, "to_do")
    }


    // ---------------------------------------------------------------------------------------------------- //
    // --------------------------------------- GET DONE TO DO LIST ---------------------------------------- //
    // ---------------------------------------------------------------------------------------------------- //

    fun getDoneToDoItemsList (callback: ((MutableList<ToDoItem>)->Unit)?) : Boolean {

        if (userDatabaseReference == null) {
            return false
        }

        Log.d(TAG, "\n\n\n GET TO DO ITEMS LIST")
        getListActionsCallback = callback

        userDatabaseReference!!.child("toDoItems").child("done").addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                Log.d(TAG, "Got todo list")

                var toDoList: MutableList<ToDoItem> = mutableListOf()

                for (td in snapshot.children) {
                    val ID = td!!.key
                    val toDo = td.getValue(ToDoItem::class.java)
                    if(toDo != null) {
                        toDo.id = ID?.toInt()!!
                        toDoList.add(toDo)
                    }
                }
                Log.d(TAG, "ToDo List length: ${toDoList.size}")
                localDoneToDoList = toDoList
                getListActionsCallback?.invoke(toDoList)
                getListActionsCallback = null
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d(TAG, "Error getting toDo list...")
                getListActionsCallback?.invoke(mutableListOf())
                getListActionsCallback = null
            }
        })
        return true
    }



    // ---------------------------------------------------------------------------------------------------- //
    // ------------------------------------------- GET TO DO LIST ----------------------------------------- //
    // ---------------------------------------------------------------------------------------------------- //

    fun getToDoItemsList (callback: ((MutableList<ToDoItem>)->Unit)?) : Boolean {

        Log.d(TAG, "\n\n\n GET TO DO ITEMS LIST")

        if (userDatabaseReference == null) {
            Log.d(TAG, "ERROR: DATABASE REFERENCE IS NULL")
            return false
        }
        getListActionsCallback = callback

        userDatabaseReference!!.child("toDoItems").child("to_do").addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                Log.d(TAG, "Got todo list")

                var toDoList: MutableList<ToDoItem> = mutableListOf()

                for (td in snapshot.children) {
                    val ID = td!!.key
                    val toDo = td.getValue(ToDoItem::class.java)
                    if(toDo != null) {
                        toDo.id = ID?.toInt()!!
                        toDoList.add(toDo)
                    }
                }
                Log.d(TAG, "ToDo List length: ${toDoList.size}")
                localToDoList = toDoList
                getListActionsCallback?.invoke(toDoList)
                getListActionsCallback = null
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d(TAG, "Error getting toDo list...")
                getListActionsCallback?.invoke(mutableListOf())
                getListActionsCallback = null
            }
        })
        return true
    }


    fun deleteToDoItem (itemId: Int, callback: (Boolean)->Unit): Boolean {

        deleteItemCallback = callback

        Log.d(TAG, "Delete item with id ${itemId}")


        var itemIndex = 0
        for (item in localToDoList){

            if (item.id == itemId){
                Log.d(TAG, "list index ${itemIndex}")
                userDatabaseReference?.child("toDoItems")?.child("to_do")?.child(itemId.toString())?.removeValue()!!.addOnCompleteListener { task ->

                    if (task.isSuccessful) {
                        Log.d(TAG, "toDo successfully deleted")
                        localToDoList.removeAt(itemIndex)
                        deleteItemCallback?.invoke(true)
                    }
                    else {
                        Log.d(TAG, "error deleting toDo")
                        deleteItemCallback?.invoke(false)
                    }
                    deleteItemCallback = null
                }
                return true
            }
            itemIndex += 1
        }

        for (item in localDoneToDoList){

            if (item.id == itemId){
                Log.d(TAG, "list index ${itemIndex}")
                userDatabaseReference?.child("toDoItems")?.child("done")?.child(itemId.toString())?.removeValue()!!.addOnCompleteListener { task ->

                    if (task.isSuccessful) {
                        Log.d(TAG, "toDo successfully deleted")

                        localDoneToDoList.removeAt(itemIndex)
                        deleteItemCallback?.invoke(true)
                    }
                    else {
                        Log.d(TAG, "error deleting toDo")
                        deleteItemCallback?.invoke(false)
                    }
                    deleteItemCallback = null
                }
                return true
            }
            itemIndex += 1
        }


        return false
    }

    fun getAllToDos (): MutableList<ToDoItem>? {

        if (userDatabaseReference == null) {
            return null
        }
        return mutableListOf()
    }



    private val createToDoItemWithID: (ToDoItem, Int, String) -> Unit =  { newToDo, ID, path ->

        Log.d(TAG, "Add ToDoItem with ID ${ID}")
        if(newToDo == null) Log.d(TAG, "Error adding ToDoItem: null")


        else {
            userDatabaseReference?.child("toDoItems")?.child(path)?.child("$ID")?.setValue(newToDo)?.addOnCompleteListener() { task ->

                if (task.isSuccessful) {
                    Log.d(TAG, "New ToDoItem successfully added!")
                    onCreateCompleteCallback?.invoke(true)
                } else {
                    Log.d(TAG, "Error adding DoToItem")
                    onCreateCompleteCallback?.invoke(false)
                }
                toDoToAdd = null
                onFreeIDFoundCallback = null
            }
        }
    }


    private val getFirstFreeIDListner = object : ValueEventListener {

        override fun onDataChange(snapshot: DataSnapshot) {

            Log.d(TAG, "FREE ID Listener")

            val doneList = snapshot.child("done")
            val toDoList = snapshot.child("to_do")

            Log.d(TAG, "${doneList}")
            Log.d(TAG, "${toDoList}")

            if(doneList.value == null && toDoList.value == null) {
                onFreeIDFoundCallback?.invoke(toDoToAdd!!, 1, "to_do")
                return
            }

            var usedID: MutableList<Int> = mutableListOf()

            if(doneList.value != null) {
                for (td in snapshot.child("done").children) {
                    usedID.add(td.key!!.toInt())
                }
            }
            if(toDoList.value != null) {
                for (td in snapshot.child("to_do").children) {
                    usedID.add(td.key!!.toInt())
                }
            }
            usedID.sort()
            val newID = usedID.max()!! +1
            Log.d(TAG, "newID: ${newID}")

            onFreeIDFoundCallback?.invoke(toDoToAdd!!, newID, "to_do")
        }

        override fun onCancelled(error: DatabaseError) {
            Log.d(TAG, "Error collecting to do data")
        }
    }

    private fun getFirstFreeID () {
        Log.d(TAG, "Get first free ID")
        userDatabaseReference?.child("toDoItems")?.addListenerForSingleValueEvent(getFirstFreeIDListner)
    }
}