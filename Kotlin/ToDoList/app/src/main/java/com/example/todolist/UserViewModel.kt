package com.example.todolist

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.example.todolist.model.ToDoItem
import com.example.todolist.model.User
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import java.security.AuthProvider


class UserViewModel: ViewModel() {

    private val TAG = "UserViewModel"

    //private val USERS_path = "User"
    //private val TODO_ITEMS_path = "toDoItems"


    private var _user: User? = null
    val user: User get() = _user!!

    private var userDatabaseReference: DatabaseReference? = null
    private var currentUser: FirebaseUser? = null

    private lateinit var auth: FirebaseAuth
    private lateinit var databaseReference: DatabaseReference

    private var loginCallback: ((Boolean) -> Unit)? = null
    private var registerCallback: ((Boolean) -> Unit)? = null
    private var getUserInfoCallback: ((Boolean) -> Unit)? = null



    init {

        Log.d(TAG, "New UserViewModel created")

        // get firebase auth reference
        auth = FirebaseAuth.getInstance()

        // get database root reference
        databaseReference = FirebaseDatabase.getInstance().reference

        if(auth.currentUser != null) {
            val userID = auth.currentUser!!.uid
            Log.d(TAG, "user is already logged")
            userDatabaseReference = databaseReference.child("User").child(userID)
        }
    }


    fun logoutUser (){
        Log.d(TAG, "Logout, Bye!")
        auth.signOut()
    }

    private fun getUserInfo (callback: ((Boolean) -> Unit)? = null) {
        Log.d(TAG, "Get User Info")
        getUserInfoCallback = callback
        userDatabaseReference?.child("info")?.addListenerForSingleValueEvent(getUserInfoListener)
    }

    fun isUserLogged() : Boolean {
        if (auth.currentUser != null) {
            return true
        }
        return false
    }



    // -------------------- AUTHENTICATION METHODS -------------------- //

    fun login (email: String, password: String, callback: (Boolean) -> Unit) {

        Log.d(TAG, "Try to login with $email and $password")

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->

            if (task.isSuccessful){
                Log.d(TAG, "Success!")
                // get the current user
                currentUser = auth.currentUser

                if(currentUser != null) {
                    val userID = auth.currentUser!!.uid

                    // get the reference to the database of the user (root/User/$userID)
                    userDatabaseReference = databaseReference.child("User").child(userID)

                    Log.d(TAG, "Try to get info about the user")
                    // get user info
                    loginCallback = callback
                    userDatabaseReference?.child("info")?.addListenerForSingleValueEvent(getUserInfoListener)
                }

                else {
                    Log.d(TAG, "Error! current user is null")
                    callback(false)
                }
            }
            else {
                Log.d(TAG, "Error! ${task.exception?.message}")

                callback(false)
            }
        }
    }


    private val getUserInfoListener = object : ValueEventListener {

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val loggedUser = dataSnapshot.getValue(User::class.java)
            _user = loggedUser
            _user?.uid = auth.currentUser?.uid
            Log.d(TAG, user.toString())

            loginCallback?.invoke(true)
            getUserInfoCallback?.invoke(true)
            getUserInfoCallback = null
            loginCallback = null
        }

        override fun onCancelled(databaseError: DatabaseError) {
            // Getting Post failed, log a message
            Log.d(TAG, "loadPost:onCancelled", databaseError.toException())
            getUserInfoCallback?.invoke(false)
            getUserInfoCallback = null
            loginCallback?.invoke(false)
            loginCallback = null
        }
    }





    fun createUser (newUser: User, password: String, callback: (Boolean) -> Unit) {

        Log.d(TAG, "Try to create new user with ${newUser.email} and $password")

        auth.createUserWithEmailAndPassword(newUser.email, password).addOnCompleteListener() { task ->

            if (task.isSuccessful) {
                Log.d(TAG, "Success!")

                currentUser = auth.currentUser
                if (currentUser != null) {

                    Log.d(TAG, "create user info")

                    val userID = currentUser!!.uid

                    // create the new user instance
                    _user = User(newUser.email, newUser.name, newUser.last_name, userID)
                    // save the callback
                    registerCallback = callback

                    // now that the user is logged, get database reference
                    userDatabaseReference = databaseReference.child("User").child(userID)

                    // write user info to database
                    userDatabaseReference?.child("info")?.setValue(user)?.addOnCompleteListener() {task ->

                        if (task.isSuccessful) {
                            Log.d(TAG, "Success!")
                            registerCallback?.invoke(true)
                        }
                        else {
                            Log.d(TAG, "Error! ${task.exception}")
                            registerCallback?.invoke(false)
                        }
                        registerCallback = null
                    }
                }
                else {
                    Log.d(TAG, "Error, user is null")
                    callback(false)
                }
            }
            else {
                Log.d(TAG, "Error! ${task.exception}")
                callback(false)
            }
        }
    }

    fun logout () {
        userDatabaseReference = null
        currentUser = null
    }

}