package com.example.todolist.model

import com.google.firebase.database.Exclude

data class User (var email: String = "",
                 var name: String = "",
                 var last_name: String = "",
                 @get:Exclude
                 var uid: String? = null) {
}