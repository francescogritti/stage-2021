package com.example.todolist.model

import com.google.firebase.database.Exclude
import java.time.Month


class Date_t(

        var day: Int = 0,
        var month: Int = 0,
        var year: Int = 0){
}



class ToDoItem (

    var title: String = "",
    var description: String = "",
    var priority: Int = 0,
    var date:Date_t = Date_t(),
    var done: Boolean = false,
    @get:Exclude var id: Int = -1
) {
}