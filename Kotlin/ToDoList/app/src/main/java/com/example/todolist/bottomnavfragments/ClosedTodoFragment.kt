package com.example.todolist.bottomnavfragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.DatabaseViewModel
import com.example.todolist.R
import com.example.todolist.adapter.DoneToDoItemsAdapter
import com.example.todolist.adapter.toDoItemsAdapter
import com.example.todolist.databinding.FragmentClosedTodoBinding
import com.example.todolist.databinding.FragmentToDoListBinding

class ClosedTodoFragment : Fragment() {


    private lateinit var _binding: FragmentClosedTodoBinding
    private val binding get() = _binding
    private lateinit var itemsAdapter: DoneToDoItemsAdapter

    private lateinit var databaseViewModel: DatabaseViewModel

    private lateinit var recyclerView: RecyclerView

    private val TAG = "doneFragment"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
       _binding = FragmentClosedTodoBinding.inflate(inflater, container, false)

        databaseViewModel = ViewModelProvider(requireActivity()).get(com.example.todolist.DatabaseViewModel::class.java)

        databaseViewModel.getDoneToDoItemsList {list ->
            Log.d(TAG, "retrieved list: ${list.size}")
            (recyclerView.adapter as DoneToDoItemsAdapter).newDataset(list)
        }

        databaseViewModel.getUserInfo() {success ->
            if(success) Log.d(TAG, "user info collect success")
            else Log.d(TAG, "user info collect error")
        }

        recyclerView = binding.doneRecyclerView



        var swipeHandler = object: SwipeToDeleteCallback(context) {

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                Log.d(TAG, "Swipe detected! direction: ${direction}")


                if(direction == 4){     // swipe left, delete
                    Log.d(TAG, viewHolder.adapterPosition.toString())

                    val itemIndex = viewHolder.adapterPosition
                    val todoItemID = databaseViewModel.localDoneToDoList[itemIndex].id

                    Log.d(TAG, "Delete to do with ID ${todoItemID}")

                    databaseViewModel.deleteToDoItem(todoItemID) {success ->
                        if(success) Log.d(TAG, "Success! deleted")
                        else Log.d(TAG, "Error! delete")

                        itemsAdapter.todoList.removeAt(itemIndex)
                        itemsAdapter.notifyItemRemoved(itemIndex)
                    }
                }
                else if (direction == 8){       // swipe right, move to done
                    Log.d(TAG, "Move to do item ")
                    val itemIndex = viewHolder.adapterPosition
                    val todoItemID = databaseViewModel.localDoneToDoList[itemIndex].id

                    databaseViewModel.moveInToDoItemWIthID(todoItemID) {
                        Log.d(TAG, "moving finished")
                        (recyclerView.adapter as DoneToDoItemsAdapter).newDataset(databaseViewModel.localToDoList)
                        (recyclerView.adapter as DoneToDoItemsAdapter).notifyItemRemoved(itemIndex)
                    }
                }
            }
        }

        val itemTouchHandler = ItemTouchHelper(swipeHandler)
        itemTouchHandler.attachToRecyclerView(recyclerView)

        itemsAdapter = DoneToDoItemsAdapter(requireContext(), mutableListOf())
        recyclerView.adapter = itemsAdapter

        return binding.root
    }
}