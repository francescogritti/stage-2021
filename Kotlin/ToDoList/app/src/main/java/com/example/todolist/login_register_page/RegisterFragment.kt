package com.example.todolist.login_register_page

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.example.todolist.databinding.FragmentRegisterBinding
import com.google.firebase.database.DatabaseReference
import kotlinx.android.synthetic.main.fragment_register.*
import androidx.fragment.app.viewModels
import com.example.todolist.UserViewModel
import com.example.todolist.activities.toDoListActivity
import com.example.todolist.model.User


class RegisterFragment : Fragment() {


    private lateinit var auth: FirebaseAuth


    private lateinit var _binding: FragmentRegisterBinding
    private val binding get() = _binding


    private val TAG: String = "Register Fragment"



    private val userViewModel: UserViewModel by viewModels()

    private lateinit var databasereference: DatabaseReference
    private lateinit var databaseUserReference: DatabaseReference


    // Override Funcs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Initialize Firebase Auth
        auth = Firebase.auth

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        _binding = FragmentRegisterBinding.inflate(inflater, container, false)


        // go back to login page
        binding.loginTextView.setOnClickListener {
            view?.findNavController()?.popBackStack()
        }


        binding.submitButton.setOnClickListener {

            var email: String = username_text_field.text.toString()
            var password: String = password_text_field.text.toString()
            var confirm_password: String = confirmPassword_text_field.text.toString()
            var name: String = name_text_field.text.toString()
            var lastname: String = lastname_text_field.text.toString()

            if(!email.isEmpty() && !password.isEmpty() && !confirm_password.isEmpty() && !name.isEmpty() && !lastname.isEmpty()){

                if(password == confirm_password){
                    val newUser = User(email, name, lastname)
                    userViewModel.createUser(newUser, password) { success ->
                        Log.d(TAG, success.toString())
                        if (userViewModel.isUserLogged()) {
                            val intent = Intent(context, toDoListActivity::class.java)
                            context?.startActivity(intent)
                        }
                    }
                } else{
                    Toast.makeText(context, "le password non coincidono", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(context, "Compilare tutti i campi", Toast.LENGTH_SHORT).show()
            }
        }
        return binding.root
    }
}