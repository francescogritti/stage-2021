package com.example.todolist.adapter
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.R
import com.example.todolist.bottomnavfragments.ClosedTodoFragmentDirections
import com.example.todolist.bottomnavfragments.ToDoListFragmentDirections
import com.example.todolist.model.ToDoItem




class DoneToDoItemsAdapter (private val context: Context, var todoList: MutableList<ToDoItem>):
    RecyclerView.Adapter<DoneToDoItemsAdapter.ItemViewHolder>() {

    private val TAG="toDoItemsAdapter"

    public fun newDataset(newDataset: MutableList<ToDoItem>) {

        Log.d(TAG, "New dataset length: ${newDataset.size}")
        this.todoList.clear()
        this.todoList.addAll(newDataset)
        notifyDataSetChanged()
    }


    class ItemViewHolder (val view: View) : RecyclerView.ViewHolder(view) {

        val titleTextView: TextView = view.findViewById(R.id.title_text_view)
        val dateTextView: TextView = view.findViewById(R.id.date_text_view)
        val priorityIcon: ImageView = view.findViewById(R.id.priority_icon)
        val detailsButon: Button = view.findViewById(R.id.details_button)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {


        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.todo_row, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    override fun getItemCount(): Int {
        Log.d(TAG, "dataset size: ${todoList.size}")
        return todoList.size
    }

    override fun onBindViewHolder(holder: DoneToDoItemsAdapter.ItemViewHolder, position: Int) {
        val item = todoList[position]
        holder.dateTextView.text = "${item.date.day}/${item.date.month}/${item.date.year}"
        holder.titleTextView.text = item.title


        var icon_path = R.drawable.priority_icon_green
        if(item.priority == 2) icon_path = R.drawable.priority_icon_yellow
        else if(item.priority == 3) icon_path = R.drawable.priority_icon_red
        val icon = ContextCompat.getDrawable(context, icon_path)
        holder.priorityIcon.setImageDrawable(icon)

        holder.detailsButon.setOnClickListener() {
            Log.d(TAG, "Button pressed!")

            val action = ClosedTodoFragmentDirections.actionClosedTodoFragmentToToDoDetailsFragment(ID = position, todoDone = 1)
            holder.view.findNavController().navigate(action)
        }
    }
}