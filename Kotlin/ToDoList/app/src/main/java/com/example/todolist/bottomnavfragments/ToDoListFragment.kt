package com.example.todolist.bottomnavfragments

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color.parseColor
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.R
import com.example.todolist.UserViewModel
import com.example.todolist.adapter.toDoItemsAdapter
import com.example.todolist.databinding.FragmentToDoListBinding
import com.example.todolist.model.ToDoItem


import android.graphics.Color
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import androidx.core.content.ContextCompat
import com.example.todolist.DatabaseViewModel


class ToDoListFragment : Fragment() {



    private lateinit var _binding: FragmentToDoListBinding
    private val binding get() = _binding
    private lateinit var itemsAdapter: toDoItemsAdapter

    private lateinit var databaseViewModel: DatabaseViewModel

    private lateinit var recyclerView: RecyclerView

    private val TAG = "todoListFragment"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
        // Initialize Firebase Auth

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_demo, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_add -> {
            Navigation.findNavController(binding.root).navigate(R.id.action_toDoListFragment2_to_newToDoFragment2)
            true
        }
        else -> {
            true
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentToDoListBinding.inflate(inflater, container, false)


        databaseViewModel = ViewModelProvider(requireActivity()).get(com.example.todolist.DatabaseViewModel::class.java)

        databaseViewModel.getToDoItemsList {list ->
            Log.d(TAG, "retrieved list: ${list.size}")
            (recyclerView.adapter as toDoItemsAdapter).newDataset(list)
        }

        databaseViewModel.getUserInfo() {success ->
            if(success)Log.d(TAG, "user info collect success")
            else Log.d(TAG, "user info collect error")
        }

        recyclerView = binding.todolistRecyclerview

        itemsAdapter = toDoItemsAdapter(requireContext(), mutableListOf())
        recyclerView.adapter = itemsAdapter


        var swipeHandler = object: SwipeToDeleteCallback(context) {

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                Log.d(TAG, "Swipe detected! direction: ${direction}")


                if(direction == 4){     // swipe left, delete
                    Log.d(TAG, viewHolder.adapterPosition.toString())

                    val itemIndex = viewHolder.adapterPosition
                    val todoItemID = databaseViewModel.localToDoList[itemIndex].id

                    Log.d(TAG, "Delete to do with ID ${todoItemID}")

                    databaseViewModel.deleteToDoItem(todoItemID ) {success ->
                       if(success) Log.d(TAG, "Success! deleted")
                        else Log.d(TAG, "Error! delete")

                        itemsAdapter.todoList.removeAt(itemIndex)
                        itemsAdapter.notifyItemRemoved(itemIndex)
                    }
                }
                else if (direction == 8){       // swipe right, move to done
                    Log.d(TAG, "Move to do item ")
                    val itemIndex = viewHolder.adapterPosition
                    val todoItemID = databaseViewModel.localToDoList[itemIndex].id

                    databaseViewModel.moveInDoneToDoItemWithID(todoItemID) {
                        Log.d(TAG, "moving finished")
                        (recyclerView.adapter as toDoItemsAdapter).newDataset(databaseViewModel.localToDoList)
                        (recyclerView.adapter as toDoItemsAdapter).notifyItemRemoved(itemIndex)
                    }
                }
            }
        }

        val itemTouchHandler = ItemTouchHelper(swipeHandler)
        itemTouchHandler.attachToRecyclerView(recyclerView)


        recyclerView.setHasFixedSize(false)
        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL))


        requireActivity().title = "To Do"

        return binding.root
    }
}




abstract class SwipeToDeleteCallback(context: Context?) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or  ItemTouchHelper.RIGHT) {


    private val bg = ColorDrawable()
    private val deleteIcon = ContextCompat.getDrawable(context!!, R.drawable.delete_icon_white)
    private val doneIcon = ContextCompat.getDrawable(context!!, R.drawable.done_icon)
    private val intrinsicWidth = deleteIcon?.intrinsicWidth
    private val intrinsicHeight = deleteIcon?.intrinsicHeight

    private val TAG = "SwipeToDeleteClass"

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {

        if(dX > 0){
            Log.d(TAG, "Right Swipe")

            val itemView = viewHolder.itemView
            val itemHeight = itemView.bottom - itemView.top
            val isCanceled = dX == 0f && !isCurrentlyActive

            // Draw the red delete background
            bg.color = Color.parseColor("#43e436")
            bg.setBounds(itemView.left, itemView.top, itemView.left+ dX.toInt(), itemView.bottom)
            bg.draw(c)

            val deleteIconTop = itemView.top + (itemHeight - intrinsicHeight!!) / 2
            val deleteIconMargin = (itemHeight - intrinsicHeight) / 2
            val deleteIconLeft = itemView.left + deleteIconMargin - intrinsicWidth!!
            val deleteIconRight = itemView.left + deleteIconMargin
            val deleteIconBottom = deleteIconTop + intrinsicHeight

            // Draw the delete icon
            doneIcon?.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom)
            doneIcon?.draw(c)
        }



        else if(dX < 0) {
            Log.d(TAG, "Left swipe")

            val itemView = viewHolder.itemView
            val itemHeight = itemView.bottom - itemView.top
            val isCanceled = dX == 0f && !isCurrentlyActive

            // Draw the red delete background
            bg.color = Color.parseColor("#f44336")
            bg.setBounds(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
            bg.draw(c)

            val deleteIconTop = itemView.top + (itemHeight - intrinsicHeight!!) / 2
            val deleteIconMargin = (itemHeight - intrinsicHeight) / 2
            val deleteIconLeft = itemView.right - deleteIconMargin - intrinsicWidth!!
            val deleteIconRight = itemView.right - deleteIconMargin
            val deleteIconBottom = deleteIconTop + intrinsicHeight

            // Draw the delete icon
            deleteIcon?.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom)
            deleteIcon?.draw(c)
        }



            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            return
        }
}